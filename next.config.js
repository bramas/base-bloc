/** @type {import('next').NextConfig} */
const withYaml = require('next-plugin-yaml');

const nextConfig = {
  reactStrictMode: true,
  swcMinify: true,
}

if(process.env.CI) {
  //options.basePath = '/2021'
}
  

module.exports = withYaml(nextConfig);


