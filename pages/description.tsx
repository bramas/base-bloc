

import styles from '../styles/Home.module.css'

// Where to fetch ANR paper: example: https://api.archives-ouvertes.fr/search/?q=anrProjectReference_s:ANR-16-CE25-0009&wt=json

export default function Description() {
  return (<div className={styles.container}>

<main className={styles.main}>
        <h1 className={styles.title}>
          BASE-BLOC Project
        </h1>
<div className={styles.grid + " "+styles.content}>
<p>
    Blockchains allow the execution of programs in a distributed manner between participants who do not know each other. From this, we saw in particular the emergence of cryptocurrencies, such as Bitcoin. However, the existing Blockchains technologies all have in common some problems, such as a high cost in terms of storage, slow transaction validation and of course very high energy consumption, for Blockchains using proof of work.
</p>
<p>
    The BASE-BLOC project aims at finding new algorithms to solve some of these problems. The first part of the project will allow to drastically reduce the amount of storage needed by Blockchains. The second part concerns the creation of a consensus algorithm based on the interactions between the participants, thus consuming a small amount of energy. Finally, the project will focus on methods allowing more general off-chain interactions, that is to say, using messages that are not added to the Blockchain.
    The project includes a theoretical analysis of the new algorithms as well as the implementation of a proof of concept.
</p>
</div>
</main>
</div>);
};