
import styles from '../styles/Home.module.css'
import { useState, useEffect } from 'react'
import content from '../content.js';

// Where to fetch ANR paper: example: 
//https://api.archives-ouvertes.fr/search/?q=anrProjectReference_s:ANR-16-CE25-0009&wt=json
//https://api.archives-ouvertes.fr/search/?q=anrProjectReference_s:ANR-22-CE25-0009


const ApiUrl = `https://api.archives-ouvertes.fr/search/?q=anrProjectReference_s:${content.ANR_REF}&wt=json`
const Fields = 'label_s,title_s,authFullName_s,conferenceTitle_s,publicationDate_tdate,comment_s,uri_s'


const Venue = ({paper}) => {
    if (paper.conferenceTitle_s) {
        return (<span className='paper-venue'>{paper.conferenceTitle_s}. </span>)
    }
    if(paper.journalTitle_s) {
        return (<span className='paper-venue'>{paper.journalTitle_s}. </span>)
    }
    if(paper.comment_s) {
        const lines = paper.comment_s.split(';')
        return (<span className='paper-venue'>{lines[1]}. </span>)
    }
    return (<span className='paper-venue'></span>)
}

const Authors = ({paper}) => {
    return <span className='paper-authors'>{paper.authFullName_s.map((author, idx) => <span key={'author-'+idx}>
        <span className={author.split(' ').map(t => t.slice(0,1)).join('-')}>
            {author}
        </span>
        {idx < paper.authFullName_s.length-1 ? ', ' : ''}
    </span>)}
    {'. '}
    </span>
}


export default function Description() {

    const [data, setData] = useState(null)
    const [isLoading, setLoading] = useState(true)

    useEffect(() => {
        fetch(ApiUrl+'&fl='+Fields)
        .then((res) => res.json())
        .then((data) => {
            setData(data.response.docs)
            setLoading(false)
        })
        .catch((err) => {
            console.log(err)
            setLoading(false)
        })
    }, [])
    return (<div className={styles.container}>

<main className={styles.main}>
        <h1 className={styles.title}>
          BASE-BLOC Project: Publications
        </h1>
<div className={styles.grid + " "+styles.content}>
<ul>
    {
        isLoading ? <p>Loading...</p> : data.map((item, idx) => {    
            return (<li key={idx}>
                    <span className='paper-title'>{item.title_s}{'. '}</span>
                    <Authors paper={item} />
                    <Venue paper={item} /> 
                    {item.publicationDate_tdate.slice(0,4)}<br/>
                <a className='paper-preprint' href={item.uri_s+'/document'} target="_blank">
                    preprint document
                </a>
            </li>)
        })
    }
</ul>
</div>
</main>
</div>);
};
