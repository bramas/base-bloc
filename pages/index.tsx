import Head from 'next/head'
import Image from 'next/image'
import styles from '../styles/Home.module.css'
import AnrLogo from './anr_logo';
import content from '../content';


// Where to fetch ANR paper: example: https://api.archives-ouvertes.fr/search/?q=anrProjectReference_s:CODE&wt=json


export default function Home() {
  return (
    <div className={styles.container}>
      <Head>
        <title>BASE-BLOC project</title>
        <meta name="description" content="Presentation fo the BASE-BLOC ANR JCJC Project by Quentin Bramas" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <h1 className={styles.title}>
          BASE-BLOC Project
        </h1>

        <p className={styles.description}>
          Better Algorithms for Secure and Efficient Blockchains.<br/>
          ANR JCJC Project ({content.ANR_REF}).<br/>
          Coordinator: <a href="https://bramas.fr" target="_blank">Quentin Bramas</a>
        </p>

        <div className={styles.grid}>
          <a href="/description" className={styles.card}>
            <h2>Description</h2>
            <p>Read the abstract of the project.</p>
          </a>

          <a
          href={`/publications`} 
          className={styles.card}>
            <h2>List of publications</h2>
            <p>See the list of pulbications associated with the project</p>
          </a>

          <a
            href="/members"
            className={styles.card}
          >
            <h2>Members</h2>
            <p>See the member of the projects.</p>
          </a>

          <a
            href="mailto:bramas@unistra.fr"
            className={styles.card}
          >
            <h2>Contact</h2>
            <p>
              Contact the coordinator of the project by mail: bramas@unistra.fr
            </p>
          </a>
        </div>
        <div className={styles.newsSection}>
          <h2>News</h2>
          <div>
            <h3>September 2023</h3>
            <p>
              A video introducing the project (in French) is available on <a href="https://savoirs.unistra.fr/innovation/base-bloc-une-solution-pour-des-blockchains-plus-vertueuses" target='_blank'>https://savoirs.unistra.fr/...</a>.
            </p>
          </div>
          <div>
            <h3>August 2023</h3>
            <p>
              A new paper has been accepted at <a href="https://brains.dnac.org/" target='_blank'>BRAINS 2023</a>:<br/>
              <strong>Computing the Heaviest Conflict-free SUB-DAG in DAG-based DLTs.</strong> Quentin Bramas (University of Strasbourg, ICUBE)
            </p>
          </div>
        </div>
        <div className={styles.footer}>
          <AnrLogo />
        </div>
      </main>
    </div>
  )
}

/*
      <footer className={styles.footer}>
        <a
          href="https://vercel.com?utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app"
          target="_blank"
          rel="noopener noreferrer"
        >
          Powered by{' '}
          <span className={styles.logo}>
            <Image src="/vercel.svg" alt="Vercel Logo" width={72} height={16} />
          </span>
        </a>
      </footer>
    */