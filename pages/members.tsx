

import styles from '../styles/Home.module.css'

// Where to fetch ANR paper: example: https://api.archives-ouvertes.fr/search/?q=anrProjectReference_s:ANR-16-CE25-0009&wt=json

export default function Description() {
  return (<div className={styles.container}>

<main className={styles.main}>
        <h1 className={styles.title}>
          BASE-BLOC Project: Members
        </h1>
<div className={styles.grid + " "+styles.content}>
<ul>
    <li>Quentin Bramas, Associate Professor, University of Strasbourg, ICUBE {'<'}<a href="https://bramas.fr" target="_blank">bramas.fr</a>{'>'}</li>
</ul>
</div>
</main>
</div>);
};